import os
import sys
import importlib

def invoke(calc_folder_path, calc_module, calc_entry_point, calc_args):

    # Load calc module dynamically by name
    path = os.path.normpath(calc_folder_path)
    path_elements = path.split(os.sep)
    module_path = '.'.join(path_elements)
    calc_module = module_path + '.' + calc_module

    try:
        if path not in sys.path:
            sys.path.insert(0, path)
        module = importlib.import_module(calc_module);
        # Get and execute the calc function
        func = getattr(module, calc_entry_point);
        result = func(calc_args)
    finally:
        if path in sys.path:
            sys.path.remove(path)

    return result
