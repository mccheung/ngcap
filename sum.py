import json
from ngcapsdk.calc_objects import *


def do_calc(ctx: CalcContext):

    # Extract the trigger parameter
    my_trigger = ctx.trigger.payload

    # Do the Sum calculation
    a = my_trigger['number1']
    b = my_trigger['number2']
    sum = a + b

    # Save output to a table called "SUM"
    result_output = {}
    result_output['sum'] = sum
    calc_output = CalcOutput(ResourceType.Generic, 'SUM', result_output)

    # Return result
    result = CalcResult()
    result.calc_outputs.append(calc_output)
    return result
