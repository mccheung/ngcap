import requests, json
from requests.auth import HTTPBasicAuth

urlstub = "http://52.252.1.78:5002/api/generic/"

headers = {
  'Authorization': 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbklkIjoiNjU3NDNmMTAtNzI4Ny0xMWVhLWJhZDQtNTNkM2RiM2UwYThiIiwidG9rZW5UeXBlIjoiUmVmcmVzaFRva2VuIiwiY3JlYXRlZEJ5IjoiYWRtaW4iLCJwcm92aWRlciI6Ik5HQ0FQIiwic2VydmljZUlkIjoibmdjYXBzZXJ2aWNlIiwicm9sZSI6WyJuZ2NhcHNlcnZpY2UiXSwiYWNscm9sZSI6Im5nY2Fwc2VydmljZSIsImlhdCI6MTU4NTU3MzY0M30.MLuSxKNrxVCO-1_isfjDBZ7mwGHwJxmxmhDzQoK134G9-wC29fGfra0fGY95xq8mwjshhvTzidoq0-tv2ChJKGZbmkvfItDjYlO9cQgnUstkq3qfuckn0FlUC9G6Sq6lHKXXtP1Pt6ORsqswaygtN5LMBFiB5XHsQxdZwo_AZoBGNrSCRGOnsxG8uYUo3Ggx6AZ0pNku7fUih3Jjea7RIvsSz6KSAFhEzutYIR6Lht2vkLBTkhPTTv6ds90b9Gb7G3279WzLl0ebgXhI6rmlEw_s-wB6fivyCYaI02vyEKtZ63wMT6HLoTmUA_PSYDwqjfYPLZoUImiNgAyJHYDj0g',
  'Content-Type': 'application/json'
#  'User-Agent':'postmanRuntime/7.24.1'
}

def send_to_ngcap_calc (documentType,name1, val1, name2, val2):
  url = urlstub + documentType
  if documentType == "operands":
    temp1 = str(val1)
    temp2 = str(val2)
  else:
    temp1 = '\"'+ val1 + '\"'
    temp2 = '\"'+ val2 + '\"'
  payload = "{\n\""+name1+"\":"+temp1+",\n\""+name2+"\":"+temp2+",\n \"cathid\" : 21114\n}" 
  print(payload)
  return requests.request("POST", url, headers=headers, data = payload)


def query_ngcap_results(documentType):
  url = urlstub + documentType + "/query"
  payload = "{\"query\" : { \"storeTime\" : {\"$gte\" : \"\" } , \"$orderBy\" : { \"storeTime\" : -1 }  } ,\r\n  \"projection\": { \"$slice\": 1}\r\n }"
  #payload = "{\r\n  \"query\": {\r\n  \"projection\": {}\r\n  }\r\n  }"
  #print(payload)
  return requests.request("POST", url, headers=headers, data = payload)

def printJSON(jsonOut, documentType):
  # r = json.loads(jsonOut.text) # equivalent to jsonOut.json()?
  r = jsonOut.json()
  if 'results' in r.keys():
    return r['results']
  else:
    return r['id']

def free_query(documentType, find_this):
  url = urlstub + documentType + "/query"
  find_this = str(find_this)
  payload = "{\r\n  \"query\": {\r\n  \"id\":  \""+find_this+"\"\r\n }\r\n }"
  #print(payload)
  return requests.request("POST", url, headers=headers, data = payload)  

#result_response = send_to_ngcap_calc('operands','number1',57,'number2',10)
this_id = printJSON( send_to_ngcap_calc('dateoperands','date1','2020-01-01','date2','2020-09-01')  ,'')

#r = printJSON( query_ngcap_results("dateoperands"),'dateoperands' )
print(this_id)
r = printJSON(free_query('dateoperands', this_id),'')
print(r)
r = printJSON( query_ngcap_results("tat"),'' )
print(r)