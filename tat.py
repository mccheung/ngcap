
import json
from calc_objects import *
from datetime import datetime


def tat_calc(ctx: CalcContext):
    my_trigger = ctx.trigger.payload
        
    a = my_trigger['date1']
    b = my_trigger['date2']
    c = my_trigger['Cathid']
    
    d1 = datetime.strptime(a, "%Y-%m-%d")
    d2 = datetime.strptime(b, "%Y-%m-%d")
     
    tat = abs((d2 - d1).days)
    result_output = {}
    result_output['tat'] = tat
    result_output['storeTime'] = str(datetime.now())
    result_output['cathid'] = c
    calc_output = CalcOutput(ResourceType.Generic, 'tat', result_output)

    result = CalcResult()
    result.calc_outputs.append(calc_output)
    return result
