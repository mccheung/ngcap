import json
from munch import *
from string import Template

def find_component(component_arr, filter_func):
    """Find components that match a specific criteria"""
    retval = []
    for c in component_arr:
        matches = list(filter(filter_func, c.code.coding))
        if len(matches) > 0:
            retval.append(c)
    return retval

def find_component_by_code(component_arr, component_system, component_code):
    """Find components that match the specific coding system and code"""
    return find_component(component_arr, lambda x: x.system.lower() == component_system.lower() and x.code.lower() == component_code.lower())

def get_fhir_obs_quantity_value(dict_obj):
    fhir_obj = munchify(dict_obj)
    return fhir_obj.valueQuantity.value

def get_fhir_obs_quantity_value_component(dict_obj, component_system, component_code):
    fhir_obj = munchify(dict_obj)

    component_arr = find_component_by_code(fhir_obj.component, component_system, component_code)
    return component_arr[0].valueQuantity.value

def create_value_quantity(value,  unit, codeForUnit, systemForUnit):
    value_quantity_template = Template("""{
          "value": $value,
          "unit": "$unit",
          "system": "$system",
          "code": "$code"
        }""")
    value_quantity_str = value_quantity_template.substitute(code=codeForUnit,  system=systemForUnit,  value=value,  unit=unit)
    value_quantity = json.loads(value_quantity_str)
    return value_quantity 

def create_component_value_quantity( code,  system,  display, value,  unit,  codeForUnit, systemForUnit):
    comp_template = Template("""{
                    \"code\": {
                    \"coding\": [{
                        \"system\": \"$system\",
                        \"code\": \"$code\",
                        \"display\": \"$display\"
                    }],
                    \"text\": \"$display\"
                    },
            \"valueQuantity\": {
                \"value\": $value,
                \"unit\": \"$unit\",
                \"system\": \"$systemForUnit\",
                \"code\": \"$codeForUnit\"
                }
    }""")

    comp_str = comp_template.substitute( code=code,  system=system,  display=display, value=value,  unit=unit,  codeForUnit=codeForUnit, systemForUnit=systemForUnit)
    comp = json.loads(comp_str)
    return comp       

def create_component_value_string( code,  system,  display, value):
    comp_template = Template("""{
          \"code\": {
                    \"coding\": [
                    {
                        \"system\": \"$system\",
                        \"code\": \"$code\",
                        \"display\": \"$display\"
                    }
                    ],
                    \"text\": \"$display\"
                },
        \"valueString\": \"$value\"
            }""")

    comp_str = comp_template.substitute(code=code,  system=system,  display=display, value=value,  unit=unit,  codeForUnit=codeForUnit)
    comp = json.loads(comp_str)
    return comp       

def create_empty_obs(code, system, display, text, reference, effectiveDateTime):
    obs_template = Template("""{
                \"resourceType\": \"Observation\",
                \"status\": \"final\",
                \"code\": {
                    \"coding\": [
                    {
                        \"system\": \"$system\",
                        \"code\": \"$code\",
                        \"display\": \"$display\"
                    }
                    ],
                    \"text\": \"$text}\"
                },
                \"subject\": {
                    \"reference\": \"$reference\"
                },
                \"effectiveDateTime\": \"$effectiveDateTime\"
            }""")
    empty_obs_str = obs_template.substitute(system=system,code=code,display=display,text=text,reference=reference,effectiveDateTime=effectiveDateTime)
    empty_obs = json.loads(empty_obs_str)
    return empty_obs

def add_value_quantity( observation,  value_quantity):
    observation['valueQuantity'] = value_quantity

def add_component( observation,  component):
    if (observation.get('component') == None):
        observation['component']=[]
    observation['component'].append(component)