from enum import Enum

class ResourceType(Enum):
    Generic = 0
    Fhir = 1

class Status(Enum):
    Completed = 0,
    TerminatedByUser = 1,
    Failed = 2

class Operation(Enum):
    Add = 0
    Update = 1
    Upsert = 2
    Delete = 3

class CalcTrigger(object):
    """Calc Trigger"""
    def __init__(self):
        self.payload = None
        self.name = ''
        self.resource_type = ResourceType.Fhir
        self.doc_type = ''

class CalcInput(object):
    """Calc Input"""
    def __init__(self):                
        self.value = None
        self.values = []

class CalcContext(object):
    """Calc Context object that is passed into every NGCAP calculation execution"""
    def __init__(self):                
        self.trigger = CalcTrigger()
        self.calc_inputs = {}

class CalcOutput(object):
    """The result of a calc to be processed by the computation engine"""
    def __init__(self, resource_type, doc_type, value):
        self.resource_type = resource_type
        self.doc_type = doc_type
        self.operation = Operation.Add
        self.value = value

class CalcResult(object):
    """Result returned from the calculation"""
    def __init__(self):                
        self.calc_outputs = []
        self.debug_logs = []
        self.calc_exec_status = Status.Completed    

def get_calc_context_obj(calc_context_dict):
    """Maps CalcContext dictionary to Python CalcContext object"""
    calc_context = CalcContext()
    trigger_details = calc_context_dict['Trigger']
    calc_context.trigger.payload = trigger_details['Payload']
    calc_context.trigger.name = trigger_details['Name']
    calc_context.trigger.resource_type = trigger_details['ResourceType']
    calc_context.trigger.doc_type = trigger_details['DocType']
        
    calc_input_dict = calc_context_dict['Inputs']
    for name, input in calc_input_dict.items():
        new_input = CalcInput()
        new_input.value = input.get('Value', None)
        new_input.values = input.get('Values', None)
        calc_context.calc_inputs[name] = new_input

    return calc_context